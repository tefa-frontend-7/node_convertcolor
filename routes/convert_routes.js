const convertController = require("../controller/convertcolor_controller")
const routes = require('express').Router()
routes.get("/:hex", convertController)
module.exports = routes