const express = require("express")
const bodyParser = require ("body-parser")
const cors = require("cors")
const app = express()
const colorRoutes = require('./routes/convert_routes')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())

app.use ("/api", colorRoutes)

app.listen(8080,() => {
    console.log("Server run on port 8080");
})
